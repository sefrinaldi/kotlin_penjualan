package com.example.penjualan.network

import com.example.penjualan.data.model.agent.ResponseAgentList
import com.example.penjualan.data.model.login.ResponseLogin
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiEndpoint {

    @FormUrlEncoded
    @POST("login_pegawai")
    fun loginUser (
        @Field("username") username: String,
        @Field("password") password: String
    ):Call<ResponseLogin>

    @GET("agen")
    fun getAgent (): Call<ResponseAgentList>
}