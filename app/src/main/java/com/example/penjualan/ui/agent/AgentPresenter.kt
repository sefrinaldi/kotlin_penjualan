package com.example.penjualan.ui.agent

import com.example.penjualan.data.model.agent.ResponseAgentList
import com.example.penjualan.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AgentPresenter(val view: AgentContract.View): AgentContract.Presenter {

    init {
        view.initActivity()
        view.initListener()
        view.onLoadingAgent(false)
    }

    override fun getAgent() {
        view.onLoadingAgent(true)
        ApiService.endpoint.getAgent().enqueue(object : Callback<ResponseAgentList>{
            override fun onFailure(call: Call<ResponseAgentList>, t: Throwable) {
                view.onLoadingAgent(false)
            }

            override fun onResponse(
                call: Call<ResponseAgentList>,
                response: Response<ResponseAgentList>
            ) {
                view.onLoadingAgent(false)
                if (response.isSuccessful) {
                    val responseAgentList: ResponseAgentList? = response.body()
                    view.onResultAgent(responseAgentList!!)
                }
            }

        })
    }
}