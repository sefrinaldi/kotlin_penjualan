package com.example.penjualan.ui.agent

import com.example.penjualan.data.model.agent.ResponseAgentList

interface AgentContract {

    interface Presenter {
        fun getAgent ()
    }

    interface View {
        fun initActivity ()
        fun initListener ()
        fun onLoadingAgent (loading: Boolean)
        fun onResultAgent (responseAgentList: ResponseAgentList)
        fun showMessage (message: String)
    }

}